#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np

ncdfFile = "../../data/CERK4.1.ARMCu.008.nc"

listVarNames = ['RCT'] # list of relevant variables.

def cloudMask(dictVars) :
    rc = dictVars['RCT']
    mask=rc*0.   # same shape as RCT
    mask[rc>0]=1 # cell is in cloud if liquid water mixing ratio is positive
    return(mask)

import matplotlib.pyplot as plt
def treatMaskUnique(dictVars,objects):
    clds_lev50 = np.unique(objects[:,50,:,:])
    clds_lev50 = clds_lev50[clds_lev50>0]
    cond_lev50 = np.zeros(objects.shape,dtype=int)
    for i in clds_lev50:
      cond_lev50 = (objects==i)+cond_lev50
    return(cond_lev50)

def treatMask(dictVars,objects):
    clds_lev50 = np.unique(objects[:,50,:,:])
    clds_lev50 = clds_lev50[clds_lev50>0]
    cond_lev50 = np.zeros(objects.shape,dtype=int)
    for i in clds_lev50:
      cond_lev50 = (objects==i)+cond_lev50
    cond_not50 = 1 - cond_lev50
    objects[np.where(cond_not50)] = 0
    return(objects)

objects, tmp = identify(ncdfFile, listVarNames, cloudMask,funcTreatMask=treatMask,name="test_treat",overwrite=True)
objects, tmp = identify(ncdfFile, listVarNames, cloudMask,funcTreatMask=treatMaskUnique,name="test_treat_unique",overwrite=True)
