# git configure
git config --global user.name "NajdaVLF"
git config --global user.email najda.villefranque@gmail.com

# git voir les dernieres modifs
git log --decorate --graph --oneline

# git voir les fichiers qui ont été modifiés depuis la dernière version enregistrée
git status

# git ajouter les fichiers modifiés à la nouvelle version
git add src/fichier_modif.py

# git enregistrer une nouvelle version
git commit -m "un petit commentaire pour expliquer ce que j'ai fait"

# git enregistrer une nouvelle version avec un tag vx.y
git tag -a v0 3ef2721 -m "ceci est la version 0"

# git revernir à une ancienne version 
## qui porte le numéro 3ef2721 d'après git log
git checkout 3ef2721 
## qui porte le tag v0 d'après git log 
git checkout v0

# git voir les différences entre les fichiers d'une ancienne version
git difftool v0

# git créer une nouvelle branche 
git checkout -b branch_najda

# git changer de branche 
git checkout master

# git fusionner la version d'une branche (branch_najda) dans une autre (master)
git checkout master
git merge branch_najda

# git supprimer une branche
git branch -d branch_najda
