#!/usr/bin/python
# -*- coding: utf-8 -*-
# Adapted from previous script 
# Now object oriented and user generic 
# 13/06/2018
# N. Villefranque
# Modifs :
## 02/2019 | Arthur Garreau | adapted to Python 3.

import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from scipy.ndimage import generate_binary_structure
#from skimage.morphology import watershed
from common_methods import *
import matplotlib.pyplot as plt
from skimage.feature import peak_local_max

def identify(ncdfFile, listVarNames, funcCalcMask, argsCalcMask=(), dimMask=(), name="objects", diagonals=True, cyclic=(-2,-1), delete=0, rename=False, criteria=None, funcTreatMask=None, argsTreatMask=(), write=True, rmbounds=False,overwrite=False,unique=False,funcWatershed=None,argsWatershed=(),deleteCores=0,localmax=False,footprint=np.ones((3,3,3)), simple_prec=True):
  """identify and labels user defined objects in field, with several mandatory and optional arugments
  mandatory arguments :
      - ncdfFile : name of netCDF file containing user data. Object fields will be written in the same file.
      - listVarNames : a list of strings of any size, containing the names of the fields use to describe objects
      - funcCalcMask : a user defined function to compute a mask (0 / 1) from physical user fields. The arguments are passed in the same order as defined in listVarNames.
  optional arguments :
      - argsCalcMask : a tuple of arguments to provide to the funcCalcMask function. Default is an empty tuple
      - dimMask : a tuple of int to select the dimensions of the first extracted variable in listVarNames, that will be the dimensions of the objects field
        -> by default, the objects field is defined along all the dimensions of the first extracted variable in listVarNames
      - name : "objects" (default) or a string to name the object field in the netCDF file
      - diagonals : True (default) or False, to consider diagonal cells as adjacent
      - cyclic : None or a tuple to identify the cyclic dimensions (default are the last 2 dimensions)
      - delete : 0 (default) or integer nb>0 to delete objects containing less than nb cells 
      - rename : True or False (default) to rename objects with contiguous labels
      - criteria : a user function that should return an integer giving the category of the object. The arguments are passed in the same order as defined in listVarNames.
      - funcTreatMask : a user function that treats the whole objects field before writing it to netCDF.
      - argsTreatMask : optional arguments that will be provided to the funcTreatMask function.
      - write : True (default) or False, to write the computed fields in the netCDF file
      - rmbounds : Apply filter to remove bounds (if original data)
      - overwrite : if True, the program will overwrite "object" field in ncdfFile if it already exists, otherwise, will prompt and ask user to chose to either overwrite, rename or exit
      - unique : once the objects have been identified and a treatment for cyclic boundaries or size filtering has been applied, the objects will all have the same number (1)
      - funcWatershed : a function that returns 2 masks, one with distances ("dist"), the other (binary) with cores of objects,
                        that are used to perform watershed segmentation instead of raw labelling. 
      - argsWatershed : a tuple of optional arguments to provide to funcWatershed
      - deleteCores   : same as delete but for cores (markers) in watershed segmentation
      - localmax      : boolean (False by default) to replace watershed cores by seeds taken as local maximum of the "dist" variable
      - footprint     : array of same shape as objects, where 1 cells are considered for search of local maximum. Default np.ones((3, 3, 3))
      - simple_prec   : boolean to read arrays as float32 instead of float64 to save memory (default = True)
  """
  
  t0 = time.time()
  print ('Begin object identification')

  t1 = time.time()
  print ('Reading',listVarNames)
  dictVars,dims = read(ncdfFile, listVarNames, dimMask=dimMask, rmbounds=rmbounds, simple_prec=simple_prec)
  print ('...OK (%2.2fs)' %(time.time()-t1))
  
  t1 = time.time()
  print ('Computing mask')
  if type(argsCalcMask)!=tuple: argsCalcMask=(argsCalcMask,)
  userMask = funcCalcMask(dictVars, *argsCalcMask)
  print ('...OK (%2.2fs)' %(time.time()-t1))

  if not isinstance(funcWatershed,type(None)):
    if type(argsWatershed)!=tuple: argsWatershed=(argsWatershed,)
    t1 = time.time()
    print('Computing cores')
    userDist,userMarkers = funcWatershed(dictVars, *argsWatershed)
    print('...OK (%2.2fs)' %(time.time()-t1))
  else : userDist=None; userMarkers=None

  t1 = time.time()
  print( 'Labelling objects')
  newname = name
  objects = UserObject(userMask,diagonals,newname,dims,userDist,userMarkers,deleteCores,localmax,footprint,cyclic,rename)
  print ('...OK (%2.2fs)' %(time.time()-t1))
  
  if not isinstance(cyclic, type(None)) and isinstance(funcWatershed,type(None)) and not len(cyclic)==0 : 
   t1 = time.time()
   print ('Forcing cyclic conditions type',cyclic)
   objects.do_cyclic(cyclic,(rename&(delete==0)),objects.val)
   print ('...OK (%2.2fs)' %(time.time()-t1))
  
  if delete :
   t1 = time.time()
   print ('Deleting objects smaller than',delete)
   objects.do_delete(delete, rename)
   print ('...OK (%2.2fs)' %(time.time()-t1))
  
  if not isinstance(criteria, type(None)) : 
   t1 = time.time()
   print ('Sorting objects')
   objects.do_sort(criteria, dictVars) 
   print ('...OK (%2.2fs)' %(time.time()-t1))

  if unique: 
   t1 = time.time()
   print ('Objects are 1')
   objects.do_unique() 
   print ('...OK (%2.2fs)' %(time.time()-t1))
  
  if not isinstance(funcTreatMask, type(None)):
   t1 = time.time()
   print ('Applying treatMask to objects field')
   objects.do_apply(funcTreatMask, argsTreatMask, dictVars)
   print ('...OK (%2.2fs)' %(time.time()-t1))

  if write :
   t1 = time.time()
   print ("Writing in ncdfFile",ncdfFile)
   objects.write(ncdfFile, rmbounds, overwrite)
   print ('...OK (%2.2fs)' %(time.time()-t1))

  print ('Done. (%2.2fs)' %(time.time()-t0))
  return (objects.val, objects.type)


class UserObject() : 
    """ This class contains methods to define and treat a field of objects"""
    def __init__(self, mask, diagonals, name, dims, dist, markers, deleteCores, localmax, footprint,cyclic,rename) :
        self.dim = len(mask.shape)
        if (len(dims)!=self.dim) : 
          raise StandardError("Dimensions of the mask field must match declared dimensions (by default the same as the first var in listVarNames, or specified through dimMask)")
        else : self.dims=dims
        self.name = name
        if isinstance(dist, type(None)) :   # Simple segmentation
          self.val, self.nbr = self.label(mask,diagonals)
          self.core = None
        else :
          if localmax :                     # in case watershed should be operated on seeds rather than cores
            markers = peak_local_max(-dist, indices=False, footprint=footprint, labels=markers)
            markers, nbmarkers = self.label(markers,diagonals)
          else :
            labels, nbmarkers = self.label(markers,diagonals)
            if not isinstance(cyclic, type(None)) and not len(cyclic)==0 :
              labels, nbmarkers = self.do_cyclic(cyclic,(rename&(deleteCores==0)),labels)
            if deleteCores :
              markers = delete_smaller_than(markers,labels,deleteCores)
              nbmarkers = len(np.unique(markers))-1
            else:
              markers = labels
          print('\t',nbmarkers,'cores identified')

          self.val = watershed(dist,markers=markers,mask=mask,connectivity=diagonals)
          self.core = markers
          self.nbr = len(np.unique(self.val))-1
        self.mask = mask
        self.type = None
        print ('\t',self.nbr,'objects identified')

    def label(self, mask, diag) :
        if diag : struct = generate_binary_structure(self.dim,self.dim)
        else :    struct = generate_binary_structure(self.dim,self.dim-2)
        objects,nb_objects = sp.ndimage.label(mask, structure=struct)
        return (objects, nb_objects)

    def do_cyclic(self, choice, rename, objects) : 
        if isinstance(choice,int): choice=(choice,)
        if not isinstance(choice,tuple): raise NameError(choice+" is not a valid choice for cyclic conditions.\nUse None or a tuple")
        #objects = self.val
        shape = objects.shape
        nbr = len(np.unique(objects[objects>0]))
        listObj = np.unique(objects[objects>0]).tolist()
        tmp = np.moveaxis(objects, list(choice), range(-len(choice),0)) # swap cyclic axes to last axes
        # reshape to 4D if needed
        if self.dim==4 :
          n0,n1,n2,n3 = shape
        elif self.dim==3 :
          n1,n2,n3 = shape
          n0 = 1 
          tmp = tmp[None,:,:,:]
        elif self.dim==2 :
          n2,n3 = shape
          n0 = 1 ; n1 = 1
          tmp = tmp[None,None,:,:]
        for i0 in range(n0) :
          for i1 in range(n1) :
            for i2 in range(n2) :
              # last dim is cyclic
              if (tmp[i0,i1,i2,n3-1] and tmp[i0,i1,i2,0] and (tmp[i0,i1,i2,n3-1]!=tmp[i0,i1,i2,0])) :
                  num = tmp[i0,i1,i2,n3-1]
                  tmp[tmp==num] = tmp[i0,i1,i2,0]
            if len(choice)==2 : 
              for i3 in range(n3) :
                # also one before last dim is cyclic
                if (tmp[i0,i1,n2-1,i3] and tmp[i0,i1,0,i3] and (tmp[i0,i1,n2-1,i3]!=tmp[i0,i1,0,i3])) :
                  num = tmp[i0,i1,n2-1,i3]
                  tmp[tmp==num] = tmp[i0,i1,0,i3]
        tmp = tmp.reshape(shape)
        objects = np.moveaxis(tmp, range(-len(choice),0), list(choice))
        
        if rename : 
          labs = np.unique(objects)
          objects = np.searchsorted(labs, objects)

        nbr = len(np.unique(objects[objects>0]))
        print ('\t', self.nbr - nbr, 'objects on the borders')
        self.nbr=nbr

    def do_delete(self,nbmin,rename) :
        objects = delete_smaller_than(self.mask,self.val,nbmin)
        if rename : 
            labs = np.unique(objects)
            objects = np.searchsorted(labs, objects)
        self.val = objects
        nbr = len(np.unique(objects))-1 # except 0
        print ('\t', self.nbr - nbr, 'objects were too small')
        self.nbr = nbr

    def do_sort(self, criteria, dictVars) :
        objects = self.val
        typeObj = np.zeros(objects.shape, dtype=int)
        listObj = np.unique(objects[objects>0]).tolist()
        for num in listObj : 
            typeObj[objects==num] = criteria(dictVars, np.where(objects==num))
        for t in np.unique(typeObj[typeObj>0]) :
            print ('\t',len(np.unique(objects[typeObj==t])),' objects of type',t)
        self.type = typeObj

    def do_unique(self):
        objects = self.val
        objects[objects>0]=1

    def do_apply(self, funcTreatMask, argsTreatMask, dictVars):
        objects = self.val 
        if type(argsTreatMask)!=tuple : argsTreatMask=(argsTreatMask,)
        objects = funcTreatMask(dictVars,objects,*argsTreatMask)
        self.val = objects
        self.nbr = len(np.unique(objects))-1

    def write(self, ncdfFile, rmbounds, overwrite) :
        dset = ncdf.Dataset(ncdfFile,'a')
        name = check_name_in_ncdf(self.name,dset.variables.keys(),ncdfFile,overwrite=overwrite)
       
        if name!='o' : 
            self.name = name 
            obj = dset.createVariable(self.name, np.int32, self.dims)
        
        else : 
            obj = dset.variables[self.name]
        
        if rmbounds:
          self.val = addbounds(self.val)
        if self.dim==4 : obj[:,:,:,:] = self.val
        if self.dim==3 : obj[:,:,:] = self.val
        if self.dim==2 : obj[:,:] = self.val
        if not isinstance(self.core,type(None)) :
          corename = "core_"+self.name
          name = check_name_in_ncdf(corename,dset.variables.keys(),ncdfFile,overwrite=overwrite)
          if name!='o' : corename=name; coreObj = dset.createVariable(corename, np.int32, self.dims)
          else : coreObj = dset.variables[corename]
          if rmbounds:
              self.core = addbounds(self.core)
          if self.dim==4 : coreObj[:,:,:,:] = self.core
          if self.dim==3 : coreObj[:,:,:] = self.core
          if self.dim==2 : coreObj[:,:] = self.core
        if not isinstance(self.type,type(None)) : # in case criteria was used
          typename = 'type_'+self.name
          name = check_name_in_ncdf(typename,dset.variables.keys(),ncdfFile,overwrite=overwrite)
          if name!='o' : 
              typename=name
              typeObj = dset.createVariable(typename, np.int32, self.dims)
          else : 
              typeObj = dset.variables[typename]
          
          if rmbounds:
            typeObj = addbounds(typeObj)
          if self.dim==4 : 
              typeObj[:,:,:,:] = self.type
          if self.dim==3 : 
              typeObj[:,:,:] = self.type
          if self.dim==2 : 
              typeObj[:,:] = self.type
        
        dset.close()
        
        print ('\t',self.nbr, 'objects were written')

if __name__=="__main__":
    help("identification_methods")
