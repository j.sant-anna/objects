#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np
import netCDF4 as nc

ncfile = "../../data/CERK4.1.ARMCu.008.nc"
ncfile = "/home/villefranquen/Work/codes_analyse/les2ecRad/CERK4.1.ARMCu.012.nc"

listVarsNames = ['RCT','VLEV'] # any size. The mask dimensions must be the same as first var in list


def clouds(dictVars,level) :
    rc = dictVars['RCT']
    mask=rc[:,0,:,:]*0.   # same shape as RCT minus vertical dimension
    mask[rc[:,level,:,:]>1.e-6]=1 # cell is in cloud if liquid water mixing ratio is positive
    return mask

# To read more info on the function identify, uncomment the following line
#help(identify)
objects=np.zeros((1,160,256,256))
for level in range(160):
    print level
    objects[:,level,:,:], types = identify(ncfile, listVarsNames, clouds, argsCalcMask=level, write=False, name="clouds2D", dimMask=(0,2,3))
    print np.max(objects[:,level,:,:])
dset=nc.Dataset(ncfile,"a")
rct = dset.variables["RCT"]
newvar = dset.createVariable("clouds2D",rct.dtype,rct.dimensions)
newvar[:,:,:,:] = objects[:,:,:,:]

dset.close()
